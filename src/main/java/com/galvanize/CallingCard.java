package com.galvanize;

// You should also be able to:
// TODO Add money to the card.
// TODO Check how many minutes are left.
// TODO Decrease the number of minutes left.

// When you call useMinutes, and you use more than what's remaining, that's OK. It just means there are 0 minutes left.
// When you add a dollar amount that doesn't make an even number of minutes, just ignore the decimal places:


public class CallingCard {
    private final int centsPerMinute;
    private int remainingMinutes;

    public CallingCard(int centsPerMinute) {
        this.centsPerMinute = centsPerMinute;
    }

    // add dollars to card -- convert to cents and store total minutes
    // available on card
    public void addDollars(int dollars) {
        int centsFromDollars = dollars * 100;
        remainingMinutes = centsFromDollars / centsPerMinute;
    }

    // method to use minutes on calling card
    public void useMinutes(int minutes) {
        if (minutes > remainingMinutes) {
            remainingMinutes = 0;
        } else {
            remainingMinutes -= minutes;
        }
    }

    // get minutes remaining on card
    public int getRemainingMinutes() {
        if (remainingMinutes <= 0) {
            remainingMinutes = 0;
        }
        return remainingMinutes;
    }

    @Override
    public String toString() {
        return "CallingCard{" +
                "centsPerMinute=" + centsPerMinute +
                ", remainingMinutes=" + remainingMinutes +
                '}';
    }

}

