package com.galvanize;

// TODO Create a class named CellPhone. When you instantiate a CellPhone, you should be able to pass it a CallingCard:
// With a CellPhone you should be able to:
// TODO Start a call.
// TODO Check if there is an active call.
// TODO Let the minutes tick by.
// TODO End a call.
// TODO See the call history as a string.

// TODO When you make multiple calls, it adds it to the call history:

// TODO Every time tick() is called, it must check to see if the CallingCard still has remaining minutes.
//  If there are no remaining minutes, then the call must be ended, and indicated in history.

import java.util.ArrayList;

public class CellPhone {
    private boolean isTalking;
    private boolean cutOff;
    private final CallingCard card;
    private int timeOnPhone;
    private String phoneNumber;
    private final ArrayList<String> history = new ArrayList<>();

    public CellPhone(CallingCard card) {
        this.card = card;
    }

    // method to start a call and change
    // is talking to true
    public void call(String number) {
        isTalking = true;
        phoneNumber = number;
    }

    // returns history of calls
    public ArrayList<String> getHistory() {
        return history;
    }

    // add minute for each tick for time on call
    public void tick() {
        int oneMinute = 1;
        timeOnPhone += 1;
        if (card.getRemainingMinutes() <= 1) {
            cutOff = true;
            endCall();
        }
        card.useMinutes(oneMinute);
    }

    // returns whether on call and is currently talking
    public boolean isTalking() {
        return isTalking;
    }

    // hang up phone
    public void endCall() {

        if (!cutOff) {
            if (timeOnPhone > 1) {
                history.add(phoneNumber + " (" + timeOnPhone + " minutes)");
            } else {
                history.add(phoneNumber + " (" + timeOnPhone + " minute)");
            }
        } else {
            if (timeOnPhone > 1) {
                history.add(phoneNumber + " (cut off at " + timeOnPhone + " minutes)");
            } else {
                history.add(phoneNumber + " (cut off at " + timeOnPhone + " minute)");
            }
        }
        isTalking = false;
        timeOnPhone = 0;
    }

    @Override
    public String toString() {
        return "CellPhone{" +
                "isTalking=" + isTalking +
                ", cutOff=" + cutOff +
                ", card=" + card +
                ", timeOnPhone=" + timeOnPhone +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", history=" + history +
                '}';
    }
}

